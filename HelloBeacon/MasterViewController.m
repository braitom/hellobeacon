
//
//  MasterViewController.m
//  HelloBeacon
//
//

#import "MasterViewController.h"
#import <EventKit/EventKit.h>
#import "Event.h"
#import <NSDate+Escort.h>

#define INTERVAL_TIME 300.0f

@interface MasterViewController () <CLLocationManagerDelegate>

@property (strong, nonatomic) NSDateFormatter *formatter;
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) NSUUID *proximityUUID;
@property (nonatomic) CLBeaconRegion *beaconRegion;
@property (nonatomic) NSDate *gDate;
@property (nonatomic) EKEventStore *eventStore;
@property (nonatomic) EKCalendar *defaultCalendar;
@property (nonatomic) NSDate *enterFlagDate;
@property (nonatomic) NSDate *exitFlagDate;


@end

@implementation MasterViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.eventStore = [[EKEventStore alloc] init];

    self.navigationItem.leftBarButtonItem = self.editButtonItem;

#if TARGET_IPHONE_SIMULATOR
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
#endif

    self.formatter = [NSDateFormatter new];
    [self.formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

    if ([CLLocationManager isMonitoringAvailableForClass:[CLBeaconRegion class]]) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;

        //aplex_1
        //self.proximityUUID = [[NSUUID alloc] initWithUUIDString:@"00000000-A083-1001-B000-001C4D0FB422"];

        //aplex_2
        //self.proximityUUID = [[NSUUID alloc] initWithUUIDString:@"00000000-6F6C-1001-B000-001C4DC25331"];
        
        //applex_3
        self.proximityUUID = [[NSUUID alloc] initWithUUIDString:@"00000000-FDEE-1001-B000-001C4D269BA9"];

        //flisk_pink
        //self.proximityUUID = [[NSUUID alloc] initWithUUIDString:@"00010001-DFFB-48D2-B060-D0F5A7109611"];

        self.beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:self.proximityUUID
                                                               identifier:@"jp.co.braitom.ios"];
        
        if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            // requestAlwaysAuthorizationメソッドが利用できる場合(iOS8以上の場合)
            // 位置情報の取得許可を求めるメソッド
            [self.locationManager requestAlwaysAuthorization];
        } else {
            // requestAlwaysAuthorizationメソッドが利用できない場合(iOS8未満の場合)
            [self.locationManager startMonitoringForRegion: self.beaconRegion];
        }
//        [self.locationManager startMonitoringForRegion: self.beaconRegion];
//        [self.locationManager startUpdatingLocation];
    } else {
        NSLog(@"not use iBeacon!");
    }

    self.title = NSLocalizedString(@"入退室ログ", @"入退室ログ");
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self checkEventStoreAccessForCallendar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusNotDetermined) {
        // ユーザが位置情報の使用を許可していない
        [self.locationManager requestAlwaysAuthorization];
    } else if(status == kCLAuthorizationStatusAuthorizedAlways) {
        // ユーザが位置情報の使用を常に許可している場合
        [self.locationManager startMonitoringForRegion: self.beaconRegion];
    } else if(status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        // ユーザが位置情報の使用を使用中のみ許可している場合
        [self.locationManager startMonitoringForRegion: self.beaconRegion];
    }
}

#pragma mark - Private

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Event *event = [self.fetchedResultsController objectAtIndexPath:indexPath];

    BOOL enter = [event.enter boolValue];
    NSDate *timeStamp = event.timeStamp;

    cell.textLabel.text = enter ? NSLocalizedString(@"入室", @"入室") : NSLocalizedString(@"退室", @"退室");

    cell.detailTextLabel.text = [self.formatter stringFromDate:timeStamp];
}

- (void)insertNewObjectWithEnter:(BOOL)enter
{
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    NSEntityDescription *entity = [[self.fetchedResultsController fetchRequest] entity];
    Event *newEvent = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];

    // If appropriate, configure the new managed object.
    // Normally you should use accessor methods, but using KVC here avoids the need to add a custom class to the template.

    //テストデータ
    //NSDate *now = [NSDate date];
    //now = [[NSDate date] dateBySubtractingDays:3];
    //newEvent.timeStamp = now;

    newEvent.timeStamp = [NSDate date];
    newEvent.enter = @(enter);

    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}


- (NSString *)exitedSpan
{
    NSArray *objects = [self.fetchedResultsController fetchedObjects];
    if (objects.count == 0 || objects.count == 1) {
        return @"初めての記録です。";
    }
    NSManagedObject *enter = [objects objectAtIndex:0];
    NSManagedObject *exit = [objects objectAtIndex:1];

    NSDate *enterTime = [enter valueForKey:@"timeStamp"];
    NSDate *exitTime = [exit valueForKey:@"timeStamp"];
    int diffTime = [enterTime timeIntervalSinceDate:exitTime];

    int ss = diffTime % 60;

    int mm = (diffTime -ss) / 60 % 60;
    int hh = (diffTime -ss -mm * 60) / 3600 % 3600;

    return [NSString stringWithFormat:@"%02d:%02d:%02d",hh, mm, ss];

}

- (void)DeleteLatestData
{
    NSArray *objects = [self.fetchedResultsController fetchedObjects];
    if (objects.count == 0) {
        return;
    }
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    NSManagedObject *object = [self.fetchedResultsController.fetchedObjects objectAtIndex:0];

    NSLog(@"object = %@", object);
    [context deleteObject:object];

    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}

- (void)insertNewObject:(id)sender
{
    static BOOL enter = YES;
    [self insertNewObjectWithEnter:enter];
    enter = !enter;
}

- (void)sendNotification:(NSString*)message
{
    UILocalNotification *notification = [UILocalNotification new];
    notification.fireDate = [[NSDate date] init];
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.alertBody = message;
    notification.alertAction = NSLocalizedString(@"開く", @"開く");
    notification.soundName = UILocalNotificationDefaultSoundName;

    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];

        NSError *error = nil;
        if (![context save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return NO;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[self.fetchedResultsController sections][section] name];
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];

    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];

    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeStamp" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];

    [fetchRequest setSortDescriptors:sortDescriptors];

    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:@"dateString" cacheName:@"Master"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;

	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}

    return _fetchedResultsController;
}


- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;

    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;

        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

/*
 // Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.
 
 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
 {
 // In the simplest, most efficient, case, reload the table view.
 [self.tableView reloadData];
 }
 */

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
    [self.locationManager requestStateForRegion:self.beaconRegion];
    NSLog(@"called requestStateForRegion");
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    switch (state) {
        case CLRegionStateInside:
            if ([region isMemberOfClass:[CLBeaconRegion class]] && [CLLocationManager isRangingAvailable]) {
                [self.locationManager startRangingBeaconsInRegion:self.beaconRegion];
                NSLog(@"locationManager didDetermineState INSIDE for %@ ", region.identifier);
            }
            break;
        case CLRegionStateOutside:
            if ([region isMemberOfClass:[CLBeaconRegion class]] && [CLLocationManager isRangingAvailable]){
                [self.locationManager startRangingBeaconsInRegion:self.beaconRegion];
                NSLog(@"locationManager didDetermineState OUTSIDE for %@", region.identifier);
            }
            break;
        case CLRegionStateUnknown:
            NSLog(@"locationManager didDetermineState OTHER for %@", region.identifier);
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    NSLog(@"enter didEnterRegion");
    //    NSDate *now = [NSDate date];
    //    if ((self.enterFlagDate) != nil && [now timeIntervalSinceDate:self.enterFlagDate] < INTERVAL_TIME) {
    //        self.enterFlagDate = nil;
    //        NSLog(@"enter if");
    //        return;
    //    }

    [self enterRegion:region];
    self.enterFlagDate = [NSDate date];
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    //    NSDate *now = [NSDate date];
    //    if ((self.exitFlagDate) != nil && [now timeIntervalSinceDate:self.exitFlagDate] < INTERVAL_TIME) {
    //        self.exitFlagDate = nil;
    //        NSLog(@"exit if");
    //        return;
    //    }

    [self insertNewObjectWithEnter:NO];
    //self.exitFlagDate = [NSDate date];
    self.gDate = [NSDate date];
    NSLog(@"called didExitRegion");
}

- (void)enterRegion:(CLRegion *)region
{
    NSLog(@"enter enterRegion");
    NSLog(@"INTERVAL_TIME %f", INTERVAL_TIME);
    NSDate *now = [NSDate date];
    float diffDate = [now timeIntervalSinceDate:self.gDate];
    NSLog(@"diffDate = %f", diffDate);
    //didexit
    if (diffDate < INTERVAL_TIME) {
        NSLog(@"Enter if");
        [self DeleteLatestData];
        return;
    }

    NSArray *objects = [self.fetchedResultsController fetchedObjects];
    if (objects.count != 0 && [self checkTodayData:now]) {
        NSDate *lastLeaving = [self lastLeavingData:now];
        NSLog(@"latestLeaving = %@", lastLeaving);
        [self addEvent:now leaveDate:lastLeaving];
    }

    [self insertNewObjectWithEnter:YES];
    NSLog(@"exitedTime = %@",[self exitedSpan]);
    [self sendNotification:[NSString stringWithFormat:@"exit-enter time %@", [self exitedSpan]]];
}


-(void)accessGrantedForCalendar
{
    // Let's get the default calendar associated with our event store
    self.defaultCalendar = self.eventStore.defaultCalendarForNewEvents;
}

-(void)requestCalendarAccess
{
    [self.eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
     {
         if (granted)
         {
             MasterViewController * __weak weakSelf = self;
             // Let's ensure that our code will be executed from the main queue
             dispatch_async(dispatch_get_main_queue(), ^{
                 // The user has granted access to their Calendar; let's populate our UI with all events occuring in the next 24 hours.
                 [weakSelf accessGrantedForCalendar];
             });
         }
     }];
}


- (void)checkEventStoreAccessForCallendar
{
    EKAuthorizationStatus status = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
    switch (status) {
        case EKAuthorizationStatusAuthorized:
            [self accessGrantedForCalendar];
            break;
        case EKAuthorizationStatusNotDetermined:
            [self requestCalendarAccess];
            break;
        case EKAuthorizationStatusDenied:
        case EKAuthorizationStatusRestricted:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Privacy Warning" message:@"カレンダーへのアクセスを許可して下さい" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        default:
            break;
    }
}

- (void)addEvent:(NSDate *)comeDate leaveDate:(NSDate *) leaveDate
{
    EKEvent *comeOffice = [EKEvent eventWithEventStore:self.eventStore];
    comeOffice.title = @"出社";
    comeOffice.timeZone = [NSTimeZone defaultTimeZone];
    comeOffice.startDate = comeDate;
    comeOffice.endDate = [comeDate dateByAddingMinutes:15];
    [comeOffice setCalendar:[self.eventStore defaultCalendarForNewEvents]];

    EKEvent *leavingOffice = [EKEvent eventWithEventStore:self.eventStore];
    leavingOffice.title = @"退社";
    leavingOffice.timeZone = [NSTimeZone defaultTimeZone];
    leavingOffice.startDate = leaveDate;
    leavingOffice.endDate = [leaveDate dateByAddingMinutes:15];
    [leavingOffice setCalendar:[self.eventStore defaultCalendarForNewEvents]];

    NSError *err;
    BOOL comeOfficeResult = [self.eventStore saveEvent:comeOffice span:EKSpanThisEvent error:&err];
    BOOL leavingOfficeResult = [self.eventStore saveEvent:leavingOffice span:EKSpanThisEvent error:&err];
    NSString *message = nil;
    if (comeOfficeResult && leavingOfficeResult) {
        message = [NSString stringWithFormat:@"勤怠情報をカレンダーへ登録しました。退勤:%@ 出勤:%@", [self.formatter stringFromDate:leaveDate], [self.formatter stringFromDate:comeDate]];
    } else {
        message = [NSString stringWithFormat:@"保存に失敗しました : %@", err];
    }
    __weak NSString *__message = message;
    [self sendNotification:__message];
    //    dispatch_async(dispatch_get_main_queue(), ^{
    //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:__message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    //        [alert show];
    //    });
}



- (void)fetchResult:(NSDate *)now request:(NSFetchRequest **)request startOfToday:(NSDate **)startOfToday context:(NSManagedObjectContext **)context
{
    *startOfToday = [[now dateAtStartOfDay] dateByAddingHours:4];

    *context = [self.fetchedResultsController managedObjectContext];

    *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event"
                                              inManagedObjectContext:self.managedObjectContext];
    [*request setEntity:entity];

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeStamp" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [*request setSortDescriptors:sortDescriptors];
}

//今日のデータがあるかどうかをチェック
-(BOOL)checkTodayData:(NSDate *)now
{
    NSDate *startOfToday;
    NSManagedObjectContext *context;
    NSFetchRequest *request;
    [self fetchResult:now request:&request startOfToday:&startOfToday context:&context];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(timeStamp > %@) AND (timeStamp <= %@)", startOfToday, now];
    [request setPredicate:predicate];

    NSArray *fetchResults = [context executeFetchRequest:request error:nil];
    BOOL hasTodayDate = (fetchResults.count == 0);
    return hasTodayDate;
}

//前出社日の最終退出時刻を取得
-(NSDate *)lastLeavingData:(NSDate *)now
{
    NSDate *startOfToday;
    NSManagedObjectContext *context;
    NSFetchRequest *request;
    [self fetchResult:now request:&request startOfToday:&startOfToday context:&context];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(timeStamp < %@)", startOfToday];
    [request setPredicate:predicate];
    NSArray *fetchResults = [context executeFetchRequest:request error:nil];
    NSManagedObject *object = fetchResults[0];
    NSDate *leavingDate = [object valueForKey:@"timeStamp"];
    return leavingDate;
}

@end