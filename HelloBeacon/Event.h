//
//  Event.h
//  HelloBeacon
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Event : NSManagedObject

@property (nonatomic, retain) NSNumber * enter;
@property (nonatomic, retain) NSDate * timeStamp;

- (NSString *)dateString;

@end
